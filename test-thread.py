import concurrent.futures
import requests
import threading
import time

thread_local = threading.local()  # Thread-local data can be stored in this object

def get_session():
  if not hasattr(thread_local, "session"):
    thread_local.session = requests.Session()  # requests.Session() is not thread safe
  return thread_local.session

def download_site(url):
  session = get_session()
  with session.get(url) as response:
    print(f"Read {len(response.content)} from {url}")

def download_all_sites(sites):
  with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    future_object = executor.submit(download_site, sites[0])  # Submit one job
    result = future_object.result()                           # block until job completes

    executor.map(download_site, sites, timeout=5)             # Apply a function to each element of an iterable
    # map() and submit() are async. But the context manager (with) will wait until completion

if __name__ == "__main__":
  sites = [
    "https://www.google.com",
    "https://www.example.com",
  ] * 80

  start_time = time.time()
  download_all_sites(sites)  # threading code
  duration = time.time() - start_time
  print(f'Took {duration} seconds')

