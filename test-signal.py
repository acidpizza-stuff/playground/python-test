import signal
import time

class LoopManager:
  running = True

  @classmethod
  def init(cls):
    signal.signal(signal.SIGINT, lambda signal, frame: cls._signal_handler())
  
  @classmethod
  def _signal_handler(cls):
    cls.running = False

LoopManager.init()
while LoopManager.running:
  print('hello')
  time.sleep(1)

print('bye')