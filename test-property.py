class Celsius:
  def __init__(self, temperature=0):
    self.temperature = temperature  # use temperature setter
                                    # no () needed to call a property
  @property
  def temperature(self):            # Getter
    return self._temperature        # Actual value is stored as _temperature

  @temperature.setter
  def temperature(self, value):     # Setter with constraints
    if value < -273.15:
      raise ValueError("Temperature below -273 is not possible")
    self._temperature = value

human = Celsius(280)
print(human.temperature)
