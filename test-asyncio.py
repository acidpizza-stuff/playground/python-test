import asyncio
import aiohttp
import time

async def download_site(session, url):
  async with session.get(url) as response:
    print(f'Read {response.content_length} from {url}')

async def download_all_sites(sites):
  async with aiohttp.ClientSession() as session:  # only 1 session used as all tasks run in same thread
    tasks = []
    for url in sites:
      # Tasks must be created within a coroutine
      tasks.append(asyncio.create_task(download_site(session, url)))
    await asyncio.gather(*tasks, return_exceptions=True)

if __name__ == "__main__":
  sites = [
    "https://www.google.com",
    "https://www.example.com",
  ] * 80

  start_time = time.time()
  asyncio.run(download_all_sites(sites))  # asyncio code
  duration = time.time() - start_time
  print(f'Took {duration} seconds')

