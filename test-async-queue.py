import asyncio
import random

async def produce(name: int, q: asyncio.Queue) -> None:
  item = random.randrange(1,5)
  await q.put(item)
  print(f'Producer {name} added {item} to queue.')

async def consume(name: int, q: asyncio.Queue) -> None:
  while True:
    item = await q.get()
    print(f'Consumer {name} got {item} from queue')
    await asyncio.sleep(1)
    q.task_done()

async def main(nprod: int, ncon: int):
  q = asyncio.Queue()
  producers = [asyncio.create_task(produce(n, q)) for n in range(nprod)]
  consumers = [asyncio.create_task(consume(n, q)) for n in range(ncon)]
  await asyncio.gather(*producers)
  await q.join()  # Implicitly awaits consumers also
  for c in consumers:
    c.cancel()

if __name__ == "__main__":
  nprod = 5
  ncon = 3
  asyncio.run(main(nprod, ncon))

